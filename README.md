# SleepSpindlesDetector

Simple project created as part of Signal Analysis classes. Analysing sleep spindles in EEG signal.

Sleep spindles are bursts of neural oscillatory activity that are generated during stage 2 NREM sleep in a frequency range of ~11 to 16 Hz (usually 12–14 Hz) with a duration of 0.5 seconds or greater (usually 0.5–1.5 seconds).
